var https = require('https')

module.exports = function (req, res, next) {
  var url = 'https://gitlab.com/' + req.params.url + '.json'
  https.get(url, function (gitlabResponse) {
    var responseBody = ''

    gitlabResponse.on('data', function (responseData) {
      responseBody += responseData
    })

    gitlabResponse.on('end', function () {
      var responseBodyObject = JSON.parse(responseBody)
      res.send(
        '<h1>#' + responseBodyObject.iid + ': ' + responseBodyObject.title + '</h1>' +
        '<p>' + responseBodyObject.description + '</p>'
      )
    })
  })
}

